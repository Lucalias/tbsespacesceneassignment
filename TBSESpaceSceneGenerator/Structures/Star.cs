﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    public enum StarClass
    {
        O,
        B,
        A,
        F,
        G,
        K,
        M,
        Count
    }

    class Star
    {
        public double Mass { get; set; }

        public double Radius { get; set; }

        public StarClass Class { get; set; }

        public int Temperature { get; set; }

        public String ColourDescription { get; set; }
    }
}
